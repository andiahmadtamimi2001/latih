import React from 'react'
import styled from 'styled-components'
import { NavLink } from 'react-router-dom'
import {GiNoodles,GiPretzel} from 'react-icons/gi'
import {FaPizzaSlice,FaHamburger} from 'react-icons/fa'

function Category() {
  return (
    <List>
        <Slink to={'/cuisine/Italian'} >
            <FaPizzaSlice />
            <h4>Italia</h4>
        </Slink>
        <Slink to={'/cuisine/American'}>
            <FaHamburger />
            <h4>Amerika</h4>
        </Slink>
        <Slink to={'/cuisine/Thai'}>
            <GiNoodles />
            <h4>Thailand</h4>
        </Slink>
        <Slink to={'/cuisine/French'}>
            <GiPretzel />
            <h4>Prancis</h4>
        </Slink>
    </List>
  )
}
const List = styled.div`
      display         : flex;
      justify-content : center;
      margin          : 2rem 0rem;

`
const Slink = styled(NavLink)`
    display           : flex;
    flex-direction    : column;
    justify-content   : center;
    align-items       : center;
    border-radius     : 50%;
    margin-right      : 2rem;
    text-decoration   : none;
    background        : linear-gradient(35deg,#494949,#313131);
    width             : 90px;
    height            : 90px;
    cursor            : pointer;
    transform         : scale(0.8);
    h4{
        color         : white;
        font-size     : 0.8rem;
    }
    svg{
        color         : white;
    }
    &.active{
        background    : linear-gradient(to right,#c2ebed,#31e0e8);
        svg{
            color     : white;
        }
        h4{
            color     : white;
        }
        
    }
      :hover{
        background    :linear-gradient(to right,#f27121,#e94057);
        svg{
            color     :white;
        }
        h4{
            color     : white;
        }
        
    }
    
`
export default Category
