import axios from 'axios'
import '@splidejs/react-splide/css';
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { useEffect,useState } from 'react'
import { Splide,SplideSlide } from '@splidejs/react-splide'

function Popular() {

    const [popular,setPopular] = useState([])

    useEffect(() => {
        getPopular()
    },[])

    const getPopular = async () => {
        const check = localStorage.getItem("popular")
        if (check) {
            setPopular(JSON.parse(check))
        }else{
            axios.get(`https://api.spoonacular.com/recipes/random?apiKey=${process.env.REACT_APP_API_KEY}&number=9`)
            .then(res => {
                const data = res.data
                localStorage.setItem("popular",JSON.stringify(data.recipes))
                setPopular(data.recipes)
            })
            .catch(error => {
                console.log(error);
            })
        }
        
    }
  return (
    <div>
        <Wrapper>
            <h3>Popular Picks</h3>
            <Splide
             options={{ 
                perPage:4,
                arrows:false,
                pagination:false,
                drag:'free',
                gap :'5rem'
             }} 
            >
                {popular.map((recipe) => (           
                    <SplideSlide key={recipe.id}>
                        <Card>
                            <Link to={'/recipe/' + recipe.id} >
                                <p>{recipe.title}</p>
                                <img src={recipe.image} alt={recipe.title} />
                                <Gradient />
                            </Link>
                        </Card>
                    </SplideSlide>
                )
                )} 
            </Splide>
        </Wrapper> 
    </div>
  )
}

const Wrapper = styled.div`
    margin : 4rem 0rem;
    
`;
const Card = styled.div`
    height : 250px;
    border-radius:2rem;
    overflow : hidden;
    position : relative;
    
    img{
        border-radius:2rem;
        position:absolute;
        left:0;
        width:90%;
        height:90%;
        object-fit=cover;
    }
    p{
        position:absolute;
        z-index:10;
        left:50%;
        bottom:0%;
        transform:translate(-50%,0%);
        color:white;
        width:100%;
        text-align:center;
        font-weight:600;
        font-size:1rem;
        height:40%;
        display:flex;
        justify-content:center;
        align-items:center;
    }  
`;

const Gradient = styled.div`
    z-index:3;
    position:absolute;
    width:100%;
    height:100%;
    background:linear-gradient(rgba(0,0,0,0));
`
export default Popular
