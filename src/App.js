import Pages from "./pages/Pages";
import styled from "styled-components";
import Search from "./components/Search";
import Category from "./components/Category";
import { BrowserRouter, Link } from "react-router-dom";
import { GiKnifeFork } from "react-icons/gi";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <Nav>
        <Logo to={"/"} ><GiKnifeFork />Resep</Logo>
      </Nav>
        <Search />
         <Category />  
        <Pages />
      </BrowserRouter>
    </div>
  );
}
const Logo = styled(Link)`
  text-decoration : none;
  font-size : 20px;
  font-weight : 400;
  font-family: 'Lobster Two', cursive;
`

const Nav = styled.div`
  padding : 2rem 0rem;
  display : flex;
  justify-content : flex-start;
  align-items : center;
  svg{
    font-size : 30px;
  }
`
export default App;
