import axios from 'axios'
import { useEffect,useState } from 'react'
import styled from 'styled-components'
import {motion} from 'framer-motion'
import { Link, useParams } from 'react-router-dom'
function Cuisine() {

    const [cuisine,setCuisine] = useState([])
    let params = useParams();

    useEffect(() => {
        getCuisine(params.type)
        console.log(params.type);
    },[params.type])
    
    const getCuisine = (name) => {
        axios.get(`https://api.spoonacular.com/recipes/complexSearch?apiKey=${process.env.REACT_APP_API_KEY}&cuisine=${name}`)
        .then(res => {
            const data = res.data
            setCuisine(data.results)
        })
        .catch(error => {
            console.log(error);
        })
    }

  return <Grid animate={{ opacity:1 }} initial={{ opacity:0 }} exit={{ opacity : 0 }} transition={{ duration:0.5 }}>
            {cuisine.map((data) => {
                return(
                    <Card key={data.id}>
                        <Link to={'/recipe/' + data.id}>
                            <img src={data.image} alt="" />
                            <h4>{data.title}</h4>
                        </Link>
                    </Card>
                )
            })}
        </Grid>
  
}

const Grid = styled(motion.div)`
    display:grid;
    grid-template-columns :repeat(auto-fit,minmax(20rem,1fr));
    grid-gap : 3rem;
    
`
const Card = styled.div`
   
    img{
        width :100%;
        border-radius:2rem;
        box-shadow: rgba(17, 12, 46, 0.15) 0px 48px 100px 0px;  
      }
    a{
        text-decoration:none;
    }
    h4{
        text-align:center;
        padding:1rem
    }
`

export default Cuisine
