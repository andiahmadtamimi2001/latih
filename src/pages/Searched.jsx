import axios from 'axios'
import styled from 'styled-components'
import {useEffect,useState} from 'react'
import { Link, useParams } from 'react-router-dom'

function Searched() {

    const [searchedRecipes,setSearchedRecipes] = useState([])
    let params = useParams();
    const getSearched = async (name) => {
        axios.get(`https://api.spoonacular.com/recipes/complexSearch?apiKey=${process.env.REACT_APP_API_KEY}&query=${name}`)
        .then(res => {
            const data = res.data
            setSearchedRecipes(data.results)
        })
        .catch(error => {
            console.log(error);
        })
    }
    useEffect(() => {
        getSearched(params.search)
        console.log(params.search);
    },[params.search])

  return (
    <Grid>
      {searchedRecipes.map((data) => {
        return(
            <Card key={data.id}>
                <Link to={'/recipe/' + data.id}>
                <img src={data.image} alt="" />
                <h4>{data.title}</h4>
                </Link>
            </Card>
        )
      })}
    </Grid>
  )
}
const Grid = styled.div`
    display:grid;
    grid-template-columns :repeat(auto-fit,minmax(20rem,1fr));
    grid-gap : 3rem;\
    
`
const Card = styled.div`
   
    img{
        width :100%;
        border-radius:2rem ;
        box-shadow: rgba(17, 12, 46, 0.15) 0px 48px 100px 0px;
        }
    a{
        text-decoration:none;
    }
    h4{
        text-align:center;
        padding:1rem
    }
`
export default Searched
